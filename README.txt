VDM - Taxonomy file (vdm_term_file)
=====================================
Développé pour la Ville de Montréal

-------------------------------------
Wiki
-------------------------------------
http://wiki.dims/index.php?title=Module_Drupal_%22VDM_Term_file%22

-------------------------------------
Description
-------------------------------------
Ce module permet l'exportation et l'importation de termes de taxonomie.

Un vocabulaire peut-être "contrôllé" ou non.
Voir option "Make the terms added to this vocabulary controlled."
(/admin/structure/taxonomy/VOCABULARY/edit)

Les termes ajoutés dans un vocabulaire contrôllé se verront attribuer
un ID entre 1 et 1 000 000 000 et seront exportable dans un fichier .vterms.
(/admin/structure/taxonomy/export)

Il pourront être importé dans une autre installation.
(/admin/structure/taxonomy/import)

Les termes ajoutés dans un vocabulaire non contrôlé se verront attribuer
un ID plus grand que 1 000 000 000, suivant le AUTO_INCREMENT de la table.

Les termes contrôlés conserve leurs ID afin de préserver les différentes
dépendances associées à ces termes.

À l'importation, si un terme portant un ID identique existe déjà,
le terme existant restera intouché.

Il est préférable d'activé ce module sur une installation fraîche puisqu'il
change l'AUTO_INCREMENT de la table "taxonomy_term_data".

-------------------------------------
To Do
-------------------------------------
- À l'importation, développer les opérations entre paranthèses:
  Si aucun terme portant un ID identique est présent:
  -------------------------------------
  1. Import and preserve the ID
  
  Si un terme porte un ID identique:
  -------------------------------------
  2. Don't import
  (3. Import and give the existing term a new ID)
  (4. Import and give the imported term a new ID)
  (5. Import and update the existing term)

-------------------------------------
Modules dépendants
-------------------------------------
- taxonomy
- (i18n_taxonomy)(à confirmer)

-------------------------------------
Outils externes
-------------------------------------
.vterms viewer
http://labrute.montrealnet.vdm.qc.ca/tools/vterms/

.vterms edit
Pour éditer un fichier .vterms, une méthode
broche à foin ce trouve dans le répertoire debug.

-------------------------------------
Bogues connus
-------------------------------------
- Les hooks "taxonomy_term_insert" et "entity_insert" ne sont
  pas appelés lors de l'ajout d'un terme dans un vocabulaire
  contrôlé (voir fonction "_vdm_term_file_taxonomy_term_save").
- Le champ "vdm_term_file_controlled" de la table "taxonomy_vocabulary"
  ne se supprime pas à la désinstallation.

-------------------------------------
Log
-------------------------------------
2012-03-20  7.x-1.0-beta5   Kristoffer Laurin-Racicot
2012-03-08  7.x-1.0-beta4   Kristoffer Laurin-Racicot
2012-03-06  7.x-1.0-beta3   Kristoffer Laurin-Racicot
2012-03-05  7.x-1.0-beta2   Kristoffer Laurin-Racicot
2012-03-01  7.x-1.0-beta1   Kristoffer Laurin-Racicot
